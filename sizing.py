#!/usr/bin/env python3

import math

# Ceph replication factor
# Change according to usage
# '2' or '3', defaults to 3
ceph_repl = 3

def get_info(question, a_type):
    ans = input(question)
    if a_type == 'num':
        while not ans.isdigit():
            ans = input(question)
        return int(ans)
    elif a_type == 'st':
        while not ans.lower() in ['y', 'n']:
            ans = input(question)
        return ans

def print_node_info(node_info, node_type, node_total, add_info):
    print('{} x {} Node:'.format(node_total, node_type))
    print('  CPU     : {}'.format(node_info['cpu']))
    print('  RAM     : {}'.format(node_info['ram']))
    print('  NET     : At least 2x10GbE Port')
    print('  OS DISK : 2x128GB SSD in RAID1')
    print('  STORAGE : {}'.format(node_info['storage']))
    print('  ADDITIONAL INFO : {}'.format(add_info))
    print()

def calc(cores, ram, storage):
    tot_ram = num_ram // (8*16)
    vcpu_ratio = 2
    tot_cpu = (num_cores//vcpu_ratio) // (2*20)
    req_compute_node = max(tot_ram, tot_cpu)
    if ha_need == 'y':
        if req_compute_node % 2 == 0:
            req_compute_node += 1
    compute_node = {'cpu': '2xIntel Xeon E5-2650v3 (10Cores, 20Threads)', 
                    'ram': '8x16GB RDIMM', 
                    'storage': '6x600GB 10k RPM SAS'}
    print_node_info(compute_node, 'Compute', req_compute_node, 'RAID10 Storage')

    total_storage = req_compute_node * 300
    if num_storage > total_storage:
        num_ssd = math.floor(num_disks/journal_ratio)
        num_hdd = math.ceil(num_disks*4/journal_ratio)
        stor_per_server = (num_hdd * num_disk_cap * 1000)
        stor_remain = num_storage - total_storage
        req_stor_node = math.ceil((stor_remain*ceph_repl) / stor_per_server)
        if req_stor_node == 1:
            # require at last 3 ceph nodes
            req_stor_node = 3
            print('[INFO] Ceph requires at least 3 nodes')
            print('[INFO] Setting node number to 3')

        if req_stor_node % 2 == 0:
            print('[INFO] Ceph nodes has to be in odd numbers to avoid split brain situation')
            print('[INFO] Adding 1 more node to equation')
            req_stor_node += 1
        
        while (num_hdd + num_ssd) > (num_disks - 2):
            num_hdd -= 1

        ram_stor_node = int(math.ceil(num_hdd * num_disk_cap * 1.5))
        stor_node = {'cpu': '2xIntel Xeon E5-2650v3 (10Cores, 20Threads)', 
                     'ram': '>= {}GB RDIMM'.format(ram_stor_node), 
                     'storage': '{}x200GB SSD Journal, {}x{}TB 7.2k RPM SAS'
                     .format(num_ssd, num_hdd, num_disk_cap)}

        add_info = 'Ceph Replication factor = {}\n'.format(ceph_repl)
        add_info += 'Additional storage needed for cache tiering\n'
        add_info += 'Assuming {} disks sharing 1 SSD for Journal'.format(journal_ratio)
        print_node_info(stor_node, 'Storage', req_stor_node, add_info)

        stormon_node = {'cpu': '1xIntel Xeon E5-2630v3 (8Cores, 16Threads)', 
                        'ram': '1x8GB RDIMM', 
                        'storage': '4x600GB SSD'}
        if ha_need == 'y':
            print_node_info(stormon_node, 'Storage Monitor', 3, '')
        else:
            print_node_info(stormon_node, 'Storage Monitor', 1, '')

        openatt_node = {'cpu': '1xIntel Xeon E3 (4Cores, 8Threads)', 
                        'ram': '2x8GB RDIMM', 
                        'storage': '4x600GB SSD'}
        print_node_info(openatt_node, 'Storage OpenAttic', 1, '')

        if obj_stor_need == 'y':
            obj_node = {'cpu': '1xIntel Xeon E5-2630v3 (8Cores, 16Threads)', 
                         'ram': '8x8GB RDIMM', 
                         'storage': '6x600GB 7.2k RPM SAS'}
            if ha_need == 'y':
                print_node_info(obj_node, 'Storage RADOSGW', 2, 'Redundant Deployment')
            else:
                print_node_info(obj_node, 'Storage RADOSGW', 1, '')

        #if nfs_stor_need == 'y':
        #    nfs_node = {'cpu': '1xIntel Xeon E5-2630v3 (8Cores, 16Threads)', 
        #                 'ram': '2x8GB RDIMM', 
        #                 'storage': '6x600GB 7.2k RPM SAS'}
        #    print_node_info(nfs_node, 'Storage NFS GW', 2, '')

        if cephfs_stor_need == 'y':
            cephfs_node = {'cpu': '1xIntel Xeon E5-2630v3 (8Cores, 16Threads)', 
                         'ram': '2x8GB RDIMM', 
                         'storage': '6x600GB 7.2k RPM SAS'}
            if ha_need == 'y':
                print_node_info(cephfs_node, 'Storage Metadata Server', 2, 'Active/Hot-Standby')
            else:
                print_node_info(cephfs_node, 'Storage Metadata Server', 1, 'Active/Hot-Standby')

        if iscsi_stor_need == 'y':
            iscsi_node = {'cpu': '1xIntel Xeon E5-2630v3 (8Cores, 16Threads)', 
                         'ram': '2x8GB RDIMM', 
                         'storage': '6x600GB 7.2k RPM SAS'}
            if ha_need == 'y':
                print_node_info(iscsi_node, 'Storage iSCSI GW', 2, 'Redundant Deployment')
            else:
                print_node_info(iscsi_node, 'Storage iSCSI GW', 1, 'Redundant Deployment')

    
def print_sizing():
    admin_node = {'cpu': '2xIntel Xeon E5-2630v3 (8Cores, 16Threads)', 
                  'ram': '8x4GB RDIMM', 
                  'storage': '6x500GB 7.2k RPM SAS'}
    print_node_info(admin_node, 'Admin', 1, 'RAID10 Storage')

    control_node = {'cpu': '2xIntel Xeon E5-2630v3 (8Cores, 16Threads)', 
                    'ram': '8x16GB RDIMM', 
                    'storage': '6x600GB 10k RPM SAS'}
    if ha_need == 'y':
        print_node_info(control_node, 'Controller', 3, 'RAID10 Storage')
    else:
        print_node_info(control_node, 'Controller', 1, 'RAID10 Storage')

    monit_node = {'cpu': '1xIntel Xeon E3 (4Cores, 8Threads)', 
                  'ram': '4x8GB RDIMM', 
                  'storage': '6x600GB 10k RPM SAS'}
    print_node_info(monit_node, 'Openstack Monitoring', 1, 'RAID10 Storage')

    calc(num_cores, num_ram, num_storage)


    print('Reference URL: https://www.suse.com/docrep/documents/r0ujxti4h7/suse_openstack_cloud_reference_architecture_with_dell_hardware_white_paper.pdf')
    print()

if __name__ == '__main__':
    num_cores = get_info('Target vCPUS > ', 'num')
    num_ram = get_info('Target RAM (GB) > ', 'num')
    num_storage = get_info('Target Storage (GB) > ', 'num')
    num_disks = get_info('Total Disk Slot for each Ceph Node > ', 'num')
    num_disk_cap = get_info('Capacity per Disk for Ceph (TB) > ', 'num')
    journal_ratio = get_info('Journal Ratio for Ceph (4-6 is optimal) > ', 'num')
    obj_stor_need = get_info('Object Storage? [y/N] > ', 'st')
    iscsi_stor_need = get_info('iSCSI Storage? [y/N] > ', 'st')
    cephfs_stor_need = get_info('CephFS? [y/N] > ', 'st')
    ha_need = get_info('HA Setup? [y/N] > ', 'st')
    #nfs_stor_need = get_info('NFS? [y/N] > ', 'st')

    print_sizing()
