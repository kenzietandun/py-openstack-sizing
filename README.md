# py-openstack-sizing

A simple tool for openstack sizing.

Requirement:

- python3

Usage:

```
git clone https://gitlab.com/kenzietandun/py-openstack-sizing.git
cd py-openstack-sizing
./sizing.py
```

Updating:

```
cd py-openstack-sizing
git pull
```
